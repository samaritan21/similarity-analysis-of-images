## Similarity analysis of visual stimuli  

This code reads the images in the `\image` directory.  
A bank of Gabor filters is applied to each image to get the vector 
representation of each image.  

Next, the dissimilarity matrix D is formed based on the Euclidean distance 
between these 
vectors. The Hierarchical Clustering Dendrogram is calculated from D.  

Multi-dimensional scaling (`scaling.py`) is applied to D to reduce the 
dimentionality to 2 and the stimuli are shown in this space to reveal any 
clustering (similarity) between images.  
