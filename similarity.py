#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  4 17:43:02 2018

@author: amir
"""

import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import warnings

from itertools import chain, product
from scipy.ndimage.filters import convolve
from scipy.spatial.distance import euclidean
from scipy.cluster.hierarchy import dendrogram, linkage
from skimage import io, transform, color
from cv2 import getGaborKernel
from tqdm import tqdm
import scaling as scale

warnings.simplefilter("ignore", category=RuntimeWarning)

pd.set_option('display.max_columns', 100)

imageset_dir = "./images"

img = dict()

stims = []
for directory, _, filenames in os.walk(imageset_dir):
    for filename in filenames:
        img[filename[:-4]] = io.imread("%s/%s" % (directory, filename))
        stims.append(filename[:-4])


num_stimuli = len(img)
print(num_stimuli)

feature_cat = ["red", "green", "blue", "grayscale"]

image_size = (150, 150, 3)

proc = dict()
for c in feature_cat:
    proc[c] = dict()

for key, image in img.items():
    scaled = transform.resize(image, image_size)
    proc["grayscale"][key] = color.rgb2gray(scaled)
    proc["red"][key]   = scaled[:,:,0]
    proc["green"][key] = scaled[:,:,1]
    proc["blue"][key]  = scaled[:,:,2]

def generate_receptive_fields_param(sizes, directions):
    rad = np.vectorize(lambda x: x / 4.0 * np.pi)(directions)
    sigma = np.vectorize(lambda x: .0036 * x**2 + .35 * x + .18)(sizes)
    freq = np.vectorize(lambda x: x / .2)(sigma)
#    freq = .1*np.ones(len(sizes))

    params = list()
    for d in np.arange(directions.size):
        for s in np.arange(sizes.size):
            params.append((freq[s], sigma[s], rad[d], directions[d], sizes[s]))

    return params

field_sizes = np.linspace(1,15,17)
orientations = np.arange(0, 180, 45)
gamma = 0.3
psi = 1.0

wavelet_param = generate_receptive_fields_param(field_sizes, orientations)
wavelets = []
for (f, s, t, _, _) in wavelet_param:
    aa = np.real(getGaborKernel((20,20), sigma=s, theta=t, lambd=(2*np.pi)/f, gamma=1, psi=psi))
    bb = np.sum(aa)
    if bb<1:
        wavelets.append(aa)
    else:
        wavelets.append((1./bb)*aa)

fig, ax = plt.subplots(nrows=4, ncols=17, figsize=(51, 12))
ax_iter = iter(list(chain.from_iterable(ax)))

for i, (wavelet, (freq, sigma, rad, direction, size)) in enumerate(zip(wavelets, wavelet_param)):
    v = np.max([np.abs(np.min(wavelet)), np.max(wavelet)])
    ax = next(ax_iter)
    _ = ax.set_title("$f=$%.2f $\sigma=$%.2f $\sphericalangle=$%d$^\circ$ $size=$%d" % (freq, sigma, direction, size), fontsize=8)
    _ = ax.imshow(wavelet, interpolation=None, cmap=plt.cm.Greys_r) #, vmin=-v, vmax=v
    ax.tick_params(which='major', labelsize=8)
    ax.grid(False)

plt.tight_layout()
#plt.savefig("kernels.eps", format="eps")
plt.show()
#sys.exit()

sample_image = proc["grayscale"][stims[1]]
plt.figure()
plt.imshow(sample_image, cmap=plt.cm.Greys_r)
plt.savefig("sample_image_primates_subset1.eps", format="eps")
fig, ax = plt.subplots(nrows=4, ncols=17, figsize=(42.5,10))
ax_iter = iter(list(chain.from_iterable(ax)))
filtered_sam = np.zeros((len(wavelets), 150, 150), dtype="float")

# calculating the Gabor feature vector for a sample image for illustration
for i, kernel in enumerate(tqdm(wavelets)):
    filtered_sam[i] = convolve(sample_image, kernel, mode='reflect', cval=0.0)
    ax = next(ax_iter)
#    ax.set_title("index=%d" % (i))
    bottom = 'on' if i > 17*3-1 else 'off'
    left = 'on' if i % 17 == 0 else 'off'
    ax.tick_params(which='major', labelsize=8, labelleft=left, labelbottom=bottom)
    ax.imshow(filtered_sam[i], cmap=plt.cm.Greys_r)

plt.savefig("kernels.eps", format="eps")
plt.show()

filt_vec = np.zeros((num_stimuli, len(wavelets)), dtype="float")
filtered      = np.zeros((num_stimuli, len(wavelets), 150, 150), dtype="float")

print("calculating the Gabor feature vector for all images")
with tqdm(total=len(wavelets)*num_stimuli) as pbar:
    for i, (key, image) in enumerate(sorted(proc["grayscale"].items())):
        for j, kernel in enumerate(wavelets):
            filtered[i, j] = convolve(image, kernel, mode="reflect", cval=0.0)
            filt_vec[i, j] = np.mean(np.abs(filtered[i, j]))
            pbar.update(1)


fig_names = proc['grayscale'].keys()
eucs = np.zeros((len(filt_vec), len(filt_vec)))
for l in range(len(filt_vec)):
    for m in range(len(filt_vec)):
        eucs[l, m] = euclidean(filt_vec[l, :],filt_vec[m, :])
euc_marks = [0., 1.5, 3., 4.5, 6, 7.5]
cax = plt.imshow(eucs, interpolation='nearest', cmap=plt.cm.pink)#norm=norm,
cb2 = plt.colorbar(cax, ticks=euc_marks, orientation='vertical')
cb2.set_label('Euclidean distance')
y_tick_marks = np.arange(len(fig_names))
x_tick_marks = np.arange(len(fig_names))
plt.xticks(x_tick_marks, fig_names, rotation=45)
plt.yticks(y_tick_marks, fig_names, rotation=45)
plt.tight_layout()
plt.xlabel('stimulus')
plt.ylabel('stimulus')
#thresh = np.max(eucs) / 2.
#for i, j in product(range(np.shape(eucs)[0]), range(np.shape(eucs)[1])):
#    plt.text(j, i, format(eucs[i,j], '.1f'),
#             horizontalalignment="center",
#             color="black" if eucs[i,j] > thresh else "white")
plt.savefig("./eclidean_heatmap.eps", format="eps", dpi=300)
plt.show()

Z = linkage(filt_vec, 'single')

# Hierarchical Clustering Dendrogram
labs = list(proc['grayscale'].keys())
plt.figure(figsize=(10, 10))
plt.xlabel('sample index')
plt.ylabel('distance')
dendrogram(
    Z,
    leaf_rotation=45.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
    labels=labs,
    color_threshold=1.7,
    count_sort='descending',
)
plt.savefig("./dendrogram.eps", format="eps", dpi=300)
plt.show()

aa, eigen = scale.cmdscale(eucs)

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

fig, ax = plt.subplots()
ax.scatter(aa[:,0], aa[:,1])
for i, txt in enumerate(labs):
    ax.annotate(txt[8:], (aa[:,0][i],aa[:,1][i]))
plt.axis('equal')
plt.xlabel('PC1')
plt.xlabel('PC2')
plt.savefig('./MDS.eps', format='eps', dpi=300)
plt.show()
